# Beng

Blog engine. Based on https://codewords.recurse.com/issues/five/building-a-web-framework-from-scratch-in-elixir

## Development

```
mix deps.get
mix run --no-halt
```

## Installation

```
dnf install elixir erlang-erl_interface
```

```
cd src
mix release
```

The binary will be placed inside `_build/dev/rel/beng/bin/beng`

Installation expected inside `/srv/beng`. Startup using systemd. Deployment… probably not Ansible.

The dev host must be reachable over ssh:

```
./deploy/deploy admin@dev.host.example --config=my_beng.conf.exs
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/beng](https://hexdocs.pm/beng).

## Post approval

In release:

```
beng eval "Beng.Moderation.interactive"
```

or, in development:

```
mix run -e Beng.Moderation.interactive --no-start
```

## Embedding

```
<iframe src="http://foobar.example/posts/mypostname" style="width: 100%;height: 100vh;" loading="lazy">
    <a href="http://foobar.example/posts/mypostname">Comments</a>
</iframe>
```
