# nix 2.3 required
with import <nixpkgs> { };

let
  packages = beam.packagesWith beam.interpreters.erlang;
in packages.mixRelease {
  pname = "beng";
  version = "0.1.0";
  
  buildInputs = [  ];

  selixir = elixir_1_12; # This doesn't seem to take an effect
  src = ../src;
  
  mixNixDeps = with pkgs;

  import ./mix_deps.nix {
    inherit lib beamPackages;
  };
}
