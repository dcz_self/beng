with import <nixpkgs> { };

let
  beng = import ./beng.nix;
in stdenv.mkDerivation {
  name = "beng_standalone";
  src = ./.;
  
  buildInputs = [ beng makeWrapper ];
  installPhase = ''
    mkdir -p $out/etc $out/bin
    cp ./standalone_config.exs $out/etc/config.exs
    makeWrapper "${beng}/bin/beng" $out/bin/beng_standalone --set BENG_CONFIG "$out/etc/config.exs" --set RELEASE_COOKIE "NOCOOKIE"
  '';
}
