CREATE TABLE IF NOT EXISTS "comments" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "name" TEXT, "content" TEXT,
    "approved" BOOLEAN,
    "inserted_at" DATETIME NOT NULL,
    "updated_at" DATETIME NOT NULL
);
CREATE TABLE IF NOT EXISTS "notified_about" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "last_time" DATETIME NOT NULL
);

