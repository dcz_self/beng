defmodule Beng.Notifications do
    import Bamboo.Email

    defp format_comments(comments) do
        command = get_config() |> Map.fetch!(:command)
        Beng.Templates.notify_review(comments, command)
    end

    defp start do
        Process.put(:state, :waiting)
        run()
    end

    defp run do
        receive do
            :newcomment -> IO.puts("new comment")
                case Process.get(:state) do
                    :waiting ->
                        Process.send_after(self(), :tally, 5 * 1000)
                        Process.put(:state, :tallying)
                    :tallying -> "keep waiting for the timer"
                    :rejected ->
                        send(self(), :tally)
                        Process.put(:state, :tallying)
                end
            :tally -> IO.puts("Tallying notifications")
                comments = get_unnotified_posts()
                
                mail = new_email(
                  to: get_config() |> Map.fetch!(:recipient),
                  from: {"Beng", "beng@myapp.com"},
                  subject: "New messages awaiting review",
                  text_body: comments |> format_comments
                )
                case Beng.Mailer.deliver_now(mail) do
                    {:error, e} ->
                        Process.put(:state, :rejected)
                        IO.puts("Failed to send")
                        e |> IO.inspect
                    _ ->
                        Process.put(:state, :waiting)
                        # Careful, this assumes comments sorted by insertion time
                        if Enum.count(comments) > 0 do
                             {:ok, comment} = Enum.fetch(comments, -1)
                             mark_notified(comment.inserted_at)
                        end
                end
            _ -> IO.puts("Unknown message")
        end
        run()
    end

    defp mark_notified(time) do
        import Ecto.Changeset
        notify = Notification
            |> Ecto.Query.first
            |> Beng.Repo.one
        case notify do
            nil -> Beng.Repo.insert(%Notification{last_time: time})
            notify -> change(notify, %{last_time: time})
                |> Beng.Repo.update
        end
    end

    defp get_unnotified_posts() do
        import Ecto.Query
        notify = Notification
            |> Ecto.Query.first
            |> Beng.Repo.one
        case notify do
            nil -> Comment
            notify ->
                time = notify.last_time
                Comment |> Ecto.Query.where([c], c.inserted_at > ^time)
        end
            |> Ecto.Query.where(approved: false)
            |> Ecto.Query.order_by(asc: :inserted_at)
            |> Beng.Repo.all
    end

    defp get_config() do
        Application.fetch_env!(:beng, __MODULE__)
            |> Map.new()
    end
    
    def start_link({:name, name}) do
        pid = spawn_link(fn -> start() end)
        Process.register(pid, name)
        # this is :ok, but what happens on failure?
        {:ok, pid}
    end
end

