defmodule Beng do
  
  use Application

  @impl true
  def start(_type, _args) do
    # List all child processes to be supervised
    {:ok, options} = Application.fetch_env(:beng, :listen)
    children = [
      {Plug.Cowboy, [scheme: :http, plug: Beng, options: options]},
      Beng.Repo,
#      {Beng.Notifications, name: Beng.Notifications},
      %{
        id: Beng.Notifications,
        start: {Beng.Notifications, :start_link , [name: Beng.Notifications]},
      },
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Beng.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def init(default_opts) do
    IO.puts "starting up Beng..."
    default_opts
  end

    def call(conn, _opts) do
      route(conn.method, conn.path_info, conn)
    end

    import Ecto.Query

    def parse(conn, opts \\ []) do
        opts = Keyword.put_new(opts, :parsers, [Plug.Parsers.URLENCODED, Plug.Parsers.MULTIPART])
        Plug.Parsers.call(conn, Plug.Parsers.init(opts))
    end

    def route("GET", ["posts", post_name], conn) do
        comments = Comment
            |> Ecto.Query.where(name: ^post_name, approved: true)
            |> order_by(asc: :inserted_at)
            |> Beng.Repo.all

        page_contents = Beng.Templates.show_posts(
            post_name,
            comments,
            :viewing
        ) |> Phoenix.HTML.Engine.encode_to_iodata!

        conn |> Plug.Conn.put_resp_content_type("text/html")
            |> Plug.Conn.send_resp(200, "#{page_contents}")
    end

    def route("POST", ["posts", post_name], conn) do
        conn = parse(conn)

        comment_body = conn.body_params["comment"]
        
        comment = %Comment{
            name: post_name,
            content: comment_body,
            approved: false,
        }
        Beng.Repo.insert(comment)
        
        send(Beng.Notifications, :newcomment)

        comments = Comment
            |> Ecto.Query.where(name: ^post_name, approved: true)
            |> order_by(asc: :inserted_at)
            |> Beng.Repo.all

        page_contents = Beng.Templates.show_posts(
            post_name,
            comments,
            :submitted
        ) |> Phoenix.HTML.Engine.encode_to_iodata!

        conn |> Plug.Conn.put_resp_content_type("text/html")
            |> Plug.Conn.send_resp(200, "#{page_contents}")
    end

    def route(_method, _path, conn) do
        conn |> Plug.Conn.send_resp(404, "Couldn't find that page, sorry!")
    end
end

defmodule Beng.Repo do
  use Ecto.Repo,
    otp_app: :beng,
    adapter: Ecto.Adapters.SQLite3
end

defmodule Beng.Moderation do

    import Ecto.Query
    import Ecto.Changeset

    def load() do
        me = :beng
        # Force start Ecto SQL apps
        {:ok, _} = Application.ensure_all_started(:ecto_sql)
    
        case Application.load(me) do
            :ok -> :ok
            {:error, {:already_loaded, ^me}} -> :ok
            err -> raise "Unknown state for application load: #{inspect(err)}"
        end

        Beng.Repo.start_link(pool_size: 2)
    end

    def show(action \\ fn(comment) -> nil end) do
        load()
        comments = Comment |> Ecto.Query.where(approved: false) |> Beng.Repo.all
        Enum.each(comments, fn(comment) ->
            IO.puts "thread: #{comment.name}"
            IO.puts "ID: #{comment.id}, #{comment.inserted_at}"
            Enum.each(
                String.split(comment.content, "\n"),
                fn(line) -> IO.puts "    #{line}" end
            )
            IO.puts ""
            action.(comment)
        end)
    end

    def approve(id) do
        load()
        comment = Comment
            |> Ecto.Query.where(id: ^id)
            |> Ecto.Query.first
            |> Beng.Repo.one
        case comment do
            nil -> raise "No such comment ID"
            comment ->
                changeset = change(comment, %{approved: true})
                Beng.Repo.update(changeset)
        end
    end

    def delete(id) do
        load()
        comment = Comment
            |> Ecto.Query.where(id: ^id)
            |> Ecto.Query.first
            |> Beng.Repo.one
        case comment do
            nil -> raise "No such comment ID"
            comment -> Beng.Repo.delete(comment)
        end
    end

    defp core(comment) do
        case "Approve, Delete, or Skip?" |> IO.gets |> String.first do
            n when n in ["d", "D"] ->
                Beng.Repo.delete(comment)
                IO.puts "Deleted"
            n when n in ["a", "A"] ->
                changeset = change(comment, %{approved: true})
                Beng.Repo.update(changeset)
                IO.puts "Approved"
            n when n in ["s", "S"] -> IO.puts "Skipped"
            _ ->
                IO.puts "invalid"
                core(comment)
        end
    end

    def interactive() do
        show(&core/1)
    end
end

