# Options for testing. Adjust for your setup.
import Config

config :beng, :listen, [port: 4001]

config :beng, Beng.Repo,
  database: "comments.sqlite3"

config :beng, Beng.Mailer,
  server: "localhost",
  port: 5587,
  tls: :if_available, # can be `:always` or `:never`, `:if_available` for development
  allowed_tls_versions: [:"tlsv1.2"], # or {:system, "ALLOWED_TLS_VERSIONS"} w/ comma seprated values (e.g. "tlsv1.1,tlsv1.2")
  tls_log_level: :error,
  tls_verify: :verify_peer, # optional, can be `:verify_peer` or `:verify_none`
  ssl: false, # can be `true`
  retries: 1

config :beng, Beng.Notifications,
  recipient: "root",
  command: "mix run --no-start -e Beng.Moderation.interactive"