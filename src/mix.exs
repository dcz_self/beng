defmodule Beng.MixProject do
  use Mix.Project

  def project do
    [
      app: :beng,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: [
        beng: [
            config_providers: [{Config.Reader, {:system, "BENG_CONFIG", ""}}]
        ]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
        mod: {Beng, []},
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
       {:ecto, "~> 3.0"},
       {:ecto_sqlite3, "~> 0.5"},
#       {:eex_html, "~> 1.0"},
#       {:eex_html, git: "https://github.com/CrowdHailer/eex_html.git", commit: "5cc0e19"},
       {:phoenix_html, "~> 3.1"},
       {:plug, "~> 1.0"},
       {:plug_cowboy, "~> 2.0"},
       {:bamboo, "~> 2.2"},
       {:bamboo_smtp, "~> 4.1"}
    ]
  end
end
